FROM node  AS angular
WORKDIR /app
COPY package.json /app/package.json
COPY . /app
RUN npm config set "@fortawesome:registry" https://npm.fontawesome.com/
RUN npm config set "//npm.fontawesome.com/:_authToken" 3EEDD803-BFFB-4645-92EE-58BF37552203
RUN npm install
RUN npm install -g @angular/cli
RUN ng build
# nginx
FROM nginx:1.17.8-alpine
COPY --from=angular /app/dist/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
