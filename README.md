# JenkinsPrueba

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.3.

# Instalaciones necesarias

1. Instalar NODE js: https://nodejs.org/es/
2. Instalar GIT: https://git-scm.com/
3. Instalar Typescript: https://www.typescriptlang.org/
4. Instalar Angular CLI: https://cli.angular.io/

# Levantar proyecto

1. Clona el repositorio `git clone https://gitlab.com/fbellod1986/prueba-angular-jenkins.git`
2. Ejecuta `ng update @angular/core @angular/cli --force` para actualizar a la ultima version de angular. Con respecto a esto, cualquier duda ver sitio [Angular Update Guide](https://update.angular.io/)
2. Ejecuta `npm install` para instalar todas las dependencias necesarias para que la aplicación pueda levantarse.
3. Ejecuta `ng serve` en la consola. Navega a `http://localhost:4200/`. La aplicación se volverá a cargar automáticamente si cambia alguno de los archivos de origen.
