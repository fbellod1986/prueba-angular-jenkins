import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'jenkins', loadChildren: () => import('./views/tarjetas/tarjetas.module').then(m => m.TarjetasModule) },
  { path: '', redirectTo: 'jenkins', pathMatch: 'full' },
  { path: '**', pathMatch: 'full', redirectTo: 'jenkins' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
